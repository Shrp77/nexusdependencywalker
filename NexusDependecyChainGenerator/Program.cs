﻿using System;
using DependencyWalker.Core;

namespace NexusDependencyChainGenerator
{
    internal class Program
    {
        private const string _MOD_URL = @"https://www.nexusmods.com/fallout4/mods/36534";

        private static void Main(string[] args)
        {
            var scanner = new Scanner();
            scanner.Scan(_MOD_URL);

            Console.ReadKey();
        }
    }
}