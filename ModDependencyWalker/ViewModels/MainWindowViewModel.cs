﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using DependencyWalker.Core.Models;

namespace ModDependencyWalker.ViewModels
{
    public class MainWindowViewModel
    {
        public string ModUrl { get; set; }
        public ICommand GenerateDependencyChainCommand { get; }
        public ObservableCollection<ModDependencyItem> Dependencies { get; }

        public MainWindowViewModel()
        {
            
        }
    }
}
