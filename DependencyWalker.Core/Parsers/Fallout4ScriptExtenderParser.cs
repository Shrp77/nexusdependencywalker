﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DependencyWalker.Core.Models;
using HtmlAgilityPack;

namespace DependencyWalker.Core.Parsers
{
    internal class Fallout4ScriptExtenderParser : IWebPageParser
    {
        private const string _URL_BASE = @"http://f4se.silverlock.org";

        public bool CanParseUrl(string url)
        {
            return url.ToLowerInvariant().StartsWith(_URL_BASE);
        }

        public ModDependencyItem CreateDependencyItem(ModDependencyItem parent)
        {
            return new DirectDownloadItem(parent);
        }

        public async Task<List<string>> GetDependenciesFromContentAsync(string url, ModDependencyItem mod)
        {
            var dependencies = new List<string>();

            if (mod is DirectDownloadItem item)
            {
                var web = new HtmlWeb();

                var htmlDoc = await web.LoadFromWebAsync(url);

                var title = htmlDoc.DocumentNode.SelectSingleNode("//head/title");

                mod.Name = title.InnerText;

                var node = htmlDoc.DocumentNode.SelectSingleNode("//u/following-sibling::a[1]");

                if (node == null)
                    return dependencies;

                var anchor = node.Attributes.SingleOrDefault(a => a.Name == "href");

                if (anchor == null)
                    return dependencies;

                item.DownloadUrl = $"{_URL_BASE}/{anchor.Value}";
            }

            return dependencies;
        }
    }
}
