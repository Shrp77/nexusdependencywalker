﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DependencyWalker.Core.Models;
using HtmlAgilityPack;

namespace DependencyWalker.Core.Parsers
{
    internal class NexusModsParser : IWebPageParser
    {
        private readonly string[] _urlRoots = {
            @"https://www.nexusmods.com/",
            @"http://www.nexusmods.com/"
        };

        public bool CanParseUrl(string url)
        {
            return _urlRoots.Any(r => url.ToLowerInvariant().StartsWith(r));
        }

        public ModDependencyItem CreateDependencyItem(ModDependencyItem parent)
        {
            return new NexusDownloadItem(parent);
        }

        public async Task<List<string>> GetDependenciesFromContentAsync(string url, ModDependencyItem mod)
        {
            var dependencies = new List<string>();

            var web = new HtmlWeb();

            var htmlDoc = await web.LoadFromWebAsync(url);

            var title = htmlDoc.DocumentNode.SelectSingleNode("//h1");

            mod.Name = title.InnerText;

            var blocks = htmlDoc.DocumentNode.SelectNodes(
                "//h3[contains(text(), 'requirements')]/" +
                "ancestor::div[contains(concat(' ', normalize-space(@class), ' '), ' tabbed-block ')]");

            if (blocks == null)
                return dependencies;

            foreach (var block in blocks)
            {
                var blockDoc = new HtmlDocument();
                blockDoc.LoadHtml(block.OuterHtml);
                var nodes = blockDoc.DocumentNode.SelectNodes(
                    "//td[contains(concat(' ', normalize-space(@class), ' '), ' table-require-name ')]/a");

                foreach (var node in nodes)
                {
                    var anchor = node.Attributes.SingleOrDefault(a => a.Name == "href");

                    if (anchor == null)
                        continue;

                    dependencies.Add(anchor.Value.Trim('/', '?'));
                }
            }

            return dependencies;
        }
    }
}