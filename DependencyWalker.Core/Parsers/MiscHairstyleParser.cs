﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DependencyWalker.Core.Models;
using HtmlAgilityPack;

namespace DependencyWalker.Core.Parsers
{
    internal class MiscHairstyleParser : IWebPageParser
    {
        private const string _URL_BASE = @"http://fo4-mischairstyle.tumblr.com";

        public bool CanParseUrl(string url)
        {
            return url.ToLowerInvariant().StartsWith(_URL_BASE);
        }

        public ModDependencyItem CreateDependencyItem(ModDependencyItem parent)
        {
            return new DirectDownloadItem(parent);
        }

        public async Task<List<string>> GetDependenciesFromContentAsync(string url, ModDependencyItem mod)
        {
            var dependencies = new List<string>();

            if (mod is DirectDownloadItem item)
            {
                var web = new HtmlWeb();

                var htmlDoc = await web.LoadFromWebAsync(url);

                var title = htmlDoc.DocumentNode.SelectSingleNode("//h2[contains(@class, 'blog-description')]");

                mod.Name = title.InnerText;

                item.DownloadUrl = url;
            }

            return dependencies;
        }
    }
}