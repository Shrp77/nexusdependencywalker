﻿using System.Collections.Generic;
using System.Linq;

namespace DependencyWalker.Core.Parsers
{
    internal class WebParserFactory
    {
        private readonly List<IWebPageParser> _parsers;

        public WebParserFactory()
        {
            _parsers = new List<IWebPageParser>
            {
                new NexusModsParser(),
                new Fallout4ScriptExtenderParser(),
                new MiscHairstyleParser()
            };
        }

        public IWebPageParser GetWebPageParser(string url)
        {
            return _parsers.SingleOrDefault(p => p.CanParseUrl(url));
        }
    }
}