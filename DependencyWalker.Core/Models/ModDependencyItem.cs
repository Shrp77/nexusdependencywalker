﻿using System.Collections.Generic;

namespace DependencyWalker.Core.Models
{
    public class ModDependencyItem
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public List<ModDependencyItem> Dependencies { get; }
        public ModDependencyItem Parent { get; }

        public ModDependencyItem(ModDependencyItem parent)
        {
            Parent = parent;
            Dependencies = new List<ModDependencyItem>();
        }

        public bool DoesUrlExistInHierarchy(string url)
        {
            var top = GetTopParent();

            return IsItemProcessed(top, url);
        }

        private bool IsItemProcessed(ModDependencyItem mod, string url)
        {
            foreach (var dependency in mod.Dependencies)
            {
                if (IsItemProcessed(dependency, url))
                    return true;
            }

            return mod.Url == url;
        }

        private ModDependencyItem GetTopParent()
        {
            var mod = this;
            while (mod.Parent != null)
                mod = mod.Parent;

            return mod;
        }

        public virtual void Download()
        {

        }
    }
}