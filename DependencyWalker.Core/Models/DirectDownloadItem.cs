﻿using System.Diagnostics;

namespace DependencyWalker.Core.Models
{
    internal class DirectDownloadItem : ModDependencyItem
    {
        public DirectDownloadItem(ModDependencyItem parent) : base(parent)
        {
        }

        public string DownloadUrl { get; set; }

        public override void Download()
        {
            Process.Start(DownloadUrl);
        }
    }
}