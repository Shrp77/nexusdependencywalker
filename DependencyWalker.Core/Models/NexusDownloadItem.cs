﻿namespace DependencyWalker.Core.Models
{
    internal class NexusDownloadItem : ModDependencyItem
    {
        public NexusDownloadItem(ModDependencyItem parent) : base(parent)
        {
        }

        public override void Download()
        {
        }
    }
}