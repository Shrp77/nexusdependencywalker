﻿using System;
using System.Threading.Tasks;
using DependencyWalker.Core.Models;
using DependencyWalker.Core.Parsers;

namespace DependencyWalker.Core
{
    public class Scanner
    {
        private readonly WebParserFactory _factory;

        public Scanner()
        {
            _factory = new WebParserFactory();
        }

        public async void Scan(string url)
        {
            var mod = await ScanWebPageAsync(url);
            PrintModDependencies(mod);
        }

        private async Task<ModDependencyItem> ScanWebPageAsync(string url)
        {
            try
            {
                var mod = await GetWebPageDependenciesAsync(null, url);
                return mod;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private async Task<ModDependencyItem> GetWebPageDependenciesAsync(ModDependencyItem parent, string url)
        {
            if ((parent?.DoesUrlExistInHierarchy(url)).GetValueOrDefault())
            {
                Console.WriteLine($"   >< Already processed {url}");
                return null;
            }

            try
            {
                var parser = _factory.GetWebPageParser(url);
                if (parser == null)
                {
                    Console.WriteLine($"Skipping: No parser for URL {url}");
                    return null;
                }

                var mod = parser.CreateDependencyItem(parent);
                mod.Url = url;

                var urls = await parser.GetDependenciesFromContentAsync(url, mod);
                Console.WriteLine($"{url} ... {urls.Count} dependencies");

                foreach (var dependencyUrl in urls)
                {
                    var dependencyMod = await GetWebPageDependenciesAsync(mod, dependencyUrl);
                    if (dependencyMod != null)
                        mod.Dependencies.Add(dependencyMod);
                }

                return mod;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        public void PrintModDependencies(ModDependencyItem mod, int indent = 0)
        {
            var spacing = "";
            for (var i = 0; i < indent * 3; i++) spacing += " ";

            Console.WriteLine($"{spacing}{mod.Name}");
            Console.WriteLine($"{spacing}{mod.Url}");

            foreach (var dependency in mod.Dependencies) PrintModDependencies(dependency, indent + 1);
        }
    }
}