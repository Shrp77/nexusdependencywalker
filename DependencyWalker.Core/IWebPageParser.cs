﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DependencyWalker.Core.Models;

namespace DependencyWalker.Core
{
    internal interface IWebPageParser
    {
        bool CanParseUrl(string url);

        ModDependencyItem CreateDependencyItem(ModDependencyItem parent);

        Task<List<string>> GetDependenciesFromContentAsync(string url, ModDependencyItem mod);
    }
}